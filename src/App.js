import React, { Component } from 'react';
import './App.css';
import {Route, Switch} from "react-router-dom";
import Register from "./containers/Register/Register";
import Login from "./containers/Login/Login";
import Layout from "./containers/Layout/Layout";
import AddNewFilm from "./components/AddNewFilm/AddNewFilm";
import FilmsList from "./containers/FilmsList/FilmsList";
import PreviewFilm from "./containers/PreviewFilm/PreviewFilm";
import FilmsByGenre from "./containers/FilmsByGenre/FilmsByGenre";

class App extends Component {
  render() {
    return (
      <Layout>
        <Switch>
          <Route path='/' exact component={FilmsList}/>
          <Route path='/register' exact component={Register}/>
          <Route path='/login' exact component={Login}/>
          <Route path='/films' exact component={FilmsList}/>
          <Route path='/films/genre/:genre' exact component={FilmsByGenre}/>
          <Route path='/films/id/:id' exact component={PreviewFilm}/>
          <Route path='/add-new-film' exact component={AddNewFilm}/>
        </Switch>
      </Layout>
    );
  }
}

export default App;


