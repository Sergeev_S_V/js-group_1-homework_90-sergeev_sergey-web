import React, {Component, Fragment} from 'react';
import {connect} from "react-redux";
import {fetchFilms} from "../../store/actions/films";
import {Col, PageHeader} from "react-bootstrap";
import Film from "../../components/Film/Film";
import config from "../../config";
import Genres from "../../components/Genres/Genres";

const pathImage = config.apiUrl + '/uploads/';
const genres = ['Action', 'Drama', 'Detective'];

class FilmsList extends Component {

  componentDidMount() {
    this.props.onFetchFilms();
  };

  renderFilms = films => {
    films.map(film => (
      <Col md={3}>
        <Film key={film._id}
              title={film.title}
              image={pathImage + film.image}
              releaseDate={film.releaseDate}
        />
      </Col>
    ))
  };

  render() {
    return(
      <Fragment>
        <PageHeader>Films</PageHeader>
        <Col md={2}>
          <aside>
            <Genres genres={genres}/>
          </aside>
        </Col>
        <Col md={10}>
          {this.props.films.length > 0
            && this.renderFilms(this.props.films)
          }
        </Col>
      </Fragment>
    );
  }
}

const mapStateToProps = state => {
  return {
    filmsList: state.films.films
  }
};

const mapDispatchToProps = dispatch => {
  return {
    onFetchFilms: () => dispatch(fetchFilms())
  }
};

// key={film._id}
// title={film.title}
// image={film.image}
// description={film.description}
// duration={film.duration}
// releaseDate={film.releaseDate}
// country={film.country}
// genre={film.genre}
// linkToTrailer={film.linkToTrailer}
// linkToFullFilm={film.linkToFullFilm}
// actors={film.actors}

export default connect(mapStateToProps, mapDispatchToProps)(FilmsList);