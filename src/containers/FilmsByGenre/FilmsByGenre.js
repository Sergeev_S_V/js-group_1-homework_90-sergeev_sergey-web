import React, {Component, Fragment} from 'react';
import {connect} from "react-redux";
import {fetchFilmsByGenre} from "../../store/actions/films";
import Film from "../../components/Film/Film";
import config from "../../config";
import {Col} from "react-bootstrap";

const pathImage = config.apiUrl + '/uploads/';

class FilmsByGenre extends Component {

  componentDidMount() {
    this.props.onFetchFilmsByGenre(this.props.match.params.id);
  };

  renderFilmsByGenre = films => {
    films.map(film => (
      <Col md={3}>
        <Film key={film._id}
              title={film.title}
              image={pathImage + film.image}
              releaseDate={film.releaseDate}
        />
      </Col>
    ))
  };

  render() {
    return(
      <Fragment>
        <Col md={12}>
          {this.props.films.length > 0
            && this.renderFilmsByGenre(this.props.films)
          }
        </Col>
      </Fragment>
    );
  }
}

const mapStateToProps = state => {
  return {
    films: state.films.films
  }
};

const mapDispatchToProps = dispatch => {
  return {
    onFetchFilmsByGenre: genre => dispatch(fetchFilmsByGenre(genre))
  }
};

export default connect(mapStateToProps, mapDispatchToProps)(FilmsByGenre);