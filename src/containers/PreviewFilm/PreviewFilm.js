import React, {Component, Fragment} from 'react';
import {connect} from "react-redux";
import {fetchOneFilm} from "../../store/actions/films";
import {Col, Image, Panel} from "react-bootstrap";
import config from "../../config";
import Actor from "../../components/Actors/Actor";

const pathImage = config.apiUrl + '/uploads/';

class PreviewFilm extends Component {

  componentDidMount() {
    this.props.onFetchOneFilm(this.props.match.params.id);
  }

  renderOneFilm = film => {
    return (
      <Panel>
        <Col md={2}>
          <Panel.Title>
            <span>{film.title}</span>
            <Image rounded src={pathImage + film.image}/>
          </Panel.Title>
          <Panel.Body>
            <span>Name: {film.title}</span>
            <span>Year: {film.releaseDate}</span>
            <span>Country: {film.country}</span>
            <span>Genre: {film.genre}</span>
          </Panel.Body>
        </Col>
        <Col md={10}>
          <Panel.Body style={{
            display: 'flex',
            flexDirection: 'column',
            justifyContent: 'space-between',
            alignItems: 'center'}}>

            <p>Description: {film.description}</p>
            <span>Duration: {film.duration}</span>
            <div>
              {film.actors.length > 0
                && film.actors.map(actor => (
                  <Col md={2} key={actor._id}>
                    <Actor firstname={actor.firstname}
                           lastname={actor.lastname}
                           photo={actor.photo}
                    />
                  </Col>
                ))
              }
            </div>
          </Panel.Body>
        </Col>
      </Panel>
    )
  };

  render() {
    return(
      <Fragment>
        <Col md={12}>
          {this.props.lookingFilm !== null
            && this.renderOneFilm(this.props.lookingFilm)
          }
        </Col>
      </Fragment>
    );
  }
}

const mapStateToProps = state => {
  return {
    lookingFilm: state.films.lookingFilm
  }
};

const mapDispatchToProps = dispatch => {
  return {
    onFetchOneFilm: filmId => dispatch(fetchOneFilm(filmId))
  }
};

export default connect(mapStateToProps, mapDispatchToProps)(PreviewFilm);