import React, {Component, Fragment} from 'react';
import {connect} from "react-redux";
import {createFilm} from "../../store/actions/films";
import {PageHeader} from "react-bootstrap";
import FilmForm from "../FilmForm/FilmForm";

class AddNewFilm extends Component {

  createFilm = async filmData => {
    await this.props.onFilmCreate(filmData);
    this.props.history.push('/');
  };

  render() {
    return(
      <Fragment>
        <PageHeader>Add new film</PageHeader>
        <FilmForm onSubmit={this.createFilm}/>
      </Fragment>
    );
  }
}

const mapDispatchToProps = dispatch => ({
  onFilmCreate: filmData => dispatch(createFilm(filmData))
});

export default connect(null, mapDispatchToProps)(AddNewFilm);