import React, {Component} from 'react';
import {Button, Col, Form, FormGroup, Panel} from "react-bootstrap";
import FormElement from "../UI/FormElement/FormElement";
import ActorsForm from "../Actors/ActorsForm/ActorsForm";
import Actor from "../Actors/Actor";

class FilmForm extends Component {

  state = {
    title: '',
    image: '',
    description: '',
    duration: '',
    releaseDate: '',
    country: '',
    genre: '',
    linkToTrailer: '',
    linkToFullFilm: '',
    actors: [],

    firstname: '',
    lastname: '',
    photo: '',
  };


  inputChangeHandler = event => {
    this.setState({[event.target.name]: event.target.value});
  };

  fileChangeHandler = event => {
    this.setState({[event.target.name]: URL.createObjectURL(event.target.files[0])});
  };

  addActorHandler = () => {
    if (this.state.photo.length > 0 && this.state.firstname.length > 0 && this.state.lastname.length > 0) {
      let copyActors = [...this.state.actors];

      const newActor = {
        photo: this.state.photo,
        firstname: this.state.firstname,
        lastname: this.state.lastname,
      };

      copyActors.push(newActor);
      this.setState({actors: copyActors});
    }
  };

  renderActors = actors => {
    return actors.map(actor => (
      <Col md={4} key={actor._id}>
        <Actor photo={actor.photo}
               firstname={actor.firstname}
               lastname={actor.lastname}
        />
      </Col>
    ));
  };

  onSubmitHandler = event => {
    event.preventDefault();

    const formData = new FormData();
    const actors = JSON.stringify(this.state.actors);
    Object.keys(this.state).forEach(key => (
      formData.append(key, this.state[key])
    ));

    formData.append('actors', actors);
    formData.delete('firstname');
    formData.delete('lastname');
    formData.delete('photo');

    this.props.onSubmit(formData);
  };

  render() {
    const genres = ['Action', 'Drama', 'Detective'];
    return(
      <Form horizontal onSubmit={this.onSubmitHandler}>

        <FormElement required
                     title='Title'
                     changeHandler={this.inputChangeHandler}
                     type='text'
                     placeholder='Title'
                     propertyName='title'
                     value={this.state.title}
        />

        <FormElement required
                     title='Description'
                     changeHandler={this.inputChangeHandler}
                     type='textarea'
                     placeholder='Description'
                     propertyName='description'
                     value={this.state.description}
        />

        <FormElement required
                     title='Image'
                     changeHandler={this.fileChangeHandler}
                     type='file'
                     propertyName='image'
        />

        <FormElement required
                     title='Duration'
                     changeHandler={this.inputChangeHandler}
                     value={this.state.duration}
                     type='text'
                     propertyName='duration'
                     placeholder='Duration'
        />

        <FormElement required
                     title='Release date'
                     changeHandler={this.inputChangeHandler}
                     value={this.state.releaseDate}
                     type='text'
                     propertyName='releaseDate'
                     placeholder='Release date'
        />

        <FormElement required
                     title='Country'
                     changeHandler={this.inputChangeHandler}
                     value={this.state.country}
                     type='text'
                     propertyName='country'
                     placeholder='Country'
        />

        <FormElement required
                     title='Genre'
                     changeHandler={this.inputChangeHandler}
                     value={this.state.genre}
                     type='select'
                     propertyName='genre'
                     options={genres}
        />

        <FormElement required
                     title='Link to trailer'
                     changeHandler={this.inputChangeHandler}
                     value={this.state.linkToTrailer}
                     type='text'
                     propertyName='linkToTrailer'
                     placeholder='Link to trailer'
        />

        <FormElement required
                     title='Link to full film'
                     changeHandler={this.inputChangeHandler}
                     value={this.state.linkToFullFilm}
                     type='text'
                     propertyName='linkToFullFilm'
                     placeholder='Link to full film'
        />

        <FormGroup>
          <Col smOffset={2} sm={10}>
            <Button bsStyle="primary" type="submit">Add film</Button>
          </Col>
        </FormGroup>

        <Col md={6}>
          <Panel.Heading><i>Actor</i></Panel.Heading>
          {this.state.actors.length > 0
            && this.renderActors(this.state.actors)
          }
        </Col>

        <ActorsForm>
          <Col md={6}>
            <Panel.Heading><i>Add actor</i></Panel.Heading>

            <FormElement required
                         title='First name'
                         placeholder='First name'
                         propertyName='firstname'
                         type='text'
                         value={this.state.firstname}
                         changeHandler={this.inputChangeHandler}

            />

            <FormElement required
                         title='Last name'
                         placeholder='Last name'
                         propertyName='lastname'
                         type='text'
                         value={this.state.lastname}
                         changeHandler={this.inputChangeHandler}

            />

            <FormElement required
                         title='Actor photo'
                         propertyName='photo'
                         type='file'
                         changeHandler={this.fileChangeHandler}

            />
            <Button onClick={this.addActorHandler}>Add actor</Button>
          </Col>
        </ActorsForm>
      </Form>
    );
  }
}

export default FilmForm;