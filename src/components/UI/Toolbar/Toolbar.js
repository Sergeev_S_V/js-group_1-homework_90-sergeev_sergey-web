import React from 'react';
import {Nav, Navbar, NavItem} from "react-bootstrap";
import {LinkContainer} from "react-router-bootstrap";
import {connect} from "react-redux";
import AnonymousMenu from "../Menus/AnonymousMenu";
import UserMenu from "../Menus/UserMenu";

const Toolbar = ({user, logout}) => (
  <Navbar>
    <Navbar.Header>
      <Navbar.Brand>
        <LinkContainer to="/" exact>
          <a>Films app</a>
        </LinkContainer>
      </Navbar.Brand>
    </Navbar.Header>
    <Nav>
      <LinkContainer to='/add-new-film'>
        <NavItem>Add new film</NavItem>
      </LinkContainer>
    </Nav>
    <Navbar.Collapse>
      {user
        ? <UserMenu logout={logout} user={user}/>
        : <AnonymousMenu/>
      }
    </Navbar.Collapse>
  </Navbar>
);

const mapStateToProps = state => ({
  user: state.users.user
});

export default connect(mapStateToProps)(Toolbar);