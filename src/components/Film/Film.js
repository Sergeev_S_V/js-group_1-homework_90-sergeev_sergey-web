import React from 'react';
import {Thumbnail} from "react-bootstrap";
import {LinkContainer} from "react-router-bootstrap";

const Film = ({_id, image, title, releaseDate}) => (
  <LinkContainer to={`/films/id/${_id}`}>
    <Thumbnail src={image} alt="242x200">
      <h3>{title}</h3>
      <p>{releaseDate}</p>
    </Thumbnail>
  </LinkContainer>
);

export default Film;