import React from 'react'
import {ListGroup, ListGroupItem} from "react-bootstrap";

const Genres = ({genres}) => (
  <ListGroup>
    {
      genres.map(genre => (
        <ListGroupItem href={`/films/genre/${genre._id}`}
                       key={genre}
                       style={{cursor: 'pointer'}}>
          {genre}
        </ListGroupItem>
      ))
    }
  </ListGroup>
);

export default Genres;