import React from 'react';
import {Thumbnail} from "react-bootstrap";

const Actor = ({firstname, photo, lastname}) => (
  <Thumbnail src={photo} alt="100x100">
    <h3>{firstname} {lastname}</h3>
  </Thumbnail>
);

export default Actor;