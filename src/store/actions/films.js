import axios from '../../axios-api';
import {
  FETCH_FILMS_BY_GENRE_SUCCESS, FETCH_FILMS_SUCCESS, FETCH_ONE_FILM_SUCCESS,
  SUCCESS_CREATE_FILM
} from "./actionTypes";
import {push} from "react-router-redux";

export const createFilm = filmData => async dispatch => {
  try {
    await axios.post(`/films`, filmData);
    dispatch(successCreateFilm(filmData));
    dispatch(push('/'));
  } catch (err) {

  }
};

const successCreateFilm = (films) => {
  return {type: SUCCESS_CREATE_FILM, films};
};

export const fetchFilms = () => async dispatch => {
  try {
    const films = await axios.get(`/films`);
    dispatch(fetchFilmsSuccess(films));
  } catch (e) {

  }
};

const fetchFilmsSuccess = films => {
  return {type: FETCH_FILMS_SUCCESS, films};
};

export const fetchOneFilm = filmId => async dispatch => {
  try {
    const film = await axios.get(`/films/id/${filmId}`);
    dispatch(fetchOneFilmSuccess(film))
  } catch (e) {

  }
};

const fetchOneFilmSuccess = film => {
  return {type: FETCH_ONE_FILM_SUCCESS, film};
};

export const fetchFilmsByGenre = genre => async dispatch => {
  try {
    const films = await axios.get(`/films/genre/${genre}`);
    dispatch(fetchFilmsByGenreSuccess(films));
  } catch (e) {

  }
};

const fetchFilmsByGenreSuccess = filmsByGenre => {
  return {type: FETCH_FILMS_BY_GENRE_SUCCESS, filmsByGenre};
};