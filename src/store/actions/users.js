import axios from '../../axios-api';
import {push} from 'react-router-redux';
import {NotificationManager} from 'react-notifications';
import {
  LOGIN_USER_FAILURE, LOGIN_USER_SUCCESS, LOGOUT_USER, REGISTER_USER_FAILURE,
  REGISTER_USER_SUCCESS
} from "./actionTypes";

const registerUserSuccess = () => {
  return {type: REGISTER_USER_SUCCESS};
};

const registerUserFailure = error => {
  return {type: REGISTER_USER_FAILURE, error};
};

export const registerUser = userData => {
  return dispatch => {
    return axios.post('/users', userData).then(
      () => {
        dispatch(registerUserSuccess());
        dispatch(push('/'));
        NotificationManager.success('Success', 'Registration successful');
      },
      error => {
        dispatch(registerUserFailure(error.response.data));
      }
    );
  };
};

const loginUserSuccess = (user, token) => {
  return {type: LOGIN_USER_SUCCESS, user, token};
};

const loginUserFailure = (error) => {
  return {type: LOGIN_USER_FAILURE, error};
};

export const loginUser = userData => dispatch => {
  return axios.post('/users/sessions', userData)
    .then(res => {
        dispatch(loginUserSuccess(res.data.user, res.data.token));
        dispatch(push('/'));
        NotificationManager.success('Success', res.data.message);
      },
      error => {
        const err = error.response ? error.response.data : {error: 'No internet connection'};
        dispatch(loginUserFailure(err));
      });
};

export const logoutUser = () => (dispatch, getState) => {
  const token = getState().users.user.token;
  const headers = {'Token': token};

  axios.delete('/users/sessions', {headers}).then(
    () => {
      dispatch({type: LOGOUT_USER});
      dispatch(push('/'));
    },
    error => console.log('Logout error')
  )
};

export const logoutExpiredUser = () => {
  return dispatch => {
    dispatch({type: LOGOUT_USER});
    dispatch(push('/login'));
    NotificationManager.error('Error', 'Your session has expired, please login again');
  }
};