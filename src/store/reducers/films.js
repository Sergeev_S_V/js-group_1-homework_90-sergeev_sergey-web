import {
  FETCH_FILMS_BY_GENRE_SUCCESS, FETCH_FILMS_SUCCESS, FETCH_ONE_FILM_SUCCESS,
  SUCCESS_CREATE_FILM
} from "../actions/actionTypes";

const initialState = {
  films: [],
  lookingFilm: null
};

const reducer = (state = initialState, action) => {
  switch (action.type) {
    case SUCCESS_CREATE_FILM:
      return {...state, films: action.films};
    case FETCH_FILMS_SUCCESS:
      return {...state, films: action.films};
    case FETCH_ONE_FILM_SUCCESS:
      return {...state, lookingFilm: action.film};
    case FETCH_FILMS_BY_GENRE_SUCCESS:
      return {...state, films: action.filmsByGenre};
    default:
      return state;
  }
};

export default reducer;